﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    Script for moving the car between the different road sections easily.
    Just for debugging purpouses.
*/

public class CarPositionHelper : MonoBehaviour
{
    public GameObject car;

    [Header("Spwanpoints")]
    public Transform startPosition;
    public Transform higwayPosition;
    public Transform debug;

    public void MoveCarToStartPosition()
    {
        car.transform.SetPositionAndRotation(startPosition.position, startPosition.rotation);
    }

    public void MoveCarToHighwayPosition()
    {
        car.transform.SetPositionAndRotation(higwayPosition.position, higwayPosition.rotation);
    }

    public void MoveCarToDebugPosition()
    {
        car.transform.SetPositionAndRotation(debug.position, debug.rotation);
    }

}
